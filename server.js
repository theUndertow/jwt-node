require('dotenv').config()

const express = require('express')
const cors = require('cors')
const jwt = require('jsonwebtoken')

const app = express()

app.use(express.json())
app.use(cors())

const posts = [
    {
        'post': 'asdfasdf'
    }
]

app.get('/posts', authenticationToken, (req, res) => {
    return res.json(posts)
})

app.post('/login', (req, res) => {
    const { email } = req.body
    const accessToken = jwt.sign({name : email}, process.env.ACCESS_TOKEN_SECRET)
    res.json({ accessToken })
})

function authenticationToken(req, res, next) {
    const token = req.headers['authorization']
    if (!token) {
        return res.sendStatus(401)
    }
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, email) => {
        if (err) {
            return res.sendStatus(403)
        }
        req.email = email
        next()
    })
}


app.listen(3000)